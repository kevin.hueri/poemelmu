import './assets/App.css';
import { HashRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/Home";

function App() {
  return (
    <HashRouter>
      <Routes>
        <Route path="/" exact element={<Home />} />
      </Routes>
    </HashRouter>
  );
}

export default App;
