import React, {useEffect} from 'react';
import '../assets/Home.css';
import { useSelector, useDispatch } from 'react-redux';
import {getPoeme} from "../store/actions/PoemeActions";

export default function Home() {
    const dataPoeme = useSelector((state) => state.poeme.data);
    console.log('dataPoeme', dataPoeme);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getPoeme());
    }, [dispatch])
    
    return(
        <div>
            <h1 className='poemeTitle'>Poème</h1>
            {/* <iframe 
                id="videoPoeme"
                title="Inline Frame Example" 
                src="https://umotion.univ-lemans.fr/video/9064-proto-2/?is_iframe=true"  
            /> */}
        </div>
    );
};