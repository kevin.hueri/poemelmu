/*
 * Import - Module
 * *************** */
import axios from "axios";

/*
 * Import types {...}
 * ****************** */
import { GET_POEME_DATA } from "./ActionTypes.js";

/*
 * Actions
 * ******* */

const url = process.env.REACT_APP_URL;
  
  // Get Poeme
  export const getPoeme = () => {
    return (dispatch) => {
      return axios
        .get(url, {
          headers: {
            'Authorization': process.env.REACT_APP_AUT,
            // 'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Origin': 'https://umotion.univ-lemans.fr/rest/poemes/',
            "Content-Type": "application/x-www-form-urlencoded",
          },
          withCredentials: true,
        })
        .then((res) => {
          dispatch({ type: GET_POEME_DATA, payload: res.data });
        })
        .catch((err) => console.log(err));
    };
  };