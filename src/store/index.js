/*
 * Import config store by react
 * **************************** */ 
import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

/*
 * Reducers
 * ******** */ 
import {PoemeReducer} from "./reducers/PoemeReducer";

/*
 * Centralisation du store (root reducers)
 * *************************************** */ 
const rootReducer = combineReducers({
    poeme: PoemeReducer,
});

/*
 * Export du store
 * *************** */ 
export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk))); //Dev
// export const store = createStore(rootReducer); //prod