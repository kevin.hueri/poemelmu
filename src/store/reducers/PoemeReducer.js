/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
  data: {},
};

/*
 * Reducers
 * ******** */

export function PoemeReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
    case Actions.GET_POEME_DATA:
      return { ...state, data: action.payload };
  }
}

/*
 * Getters
 * ******* */